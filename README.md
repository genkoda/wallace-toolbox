# Wallace Toolbox

## About

Fork of the original [Wallace Toolbox](https://gitlab.com/suborg/wallace-toolbox) with IMEI randomisation to ease IMEI change.

## wallace toolbox feature list

- `1`: ADB root (requires Busybox for the operation, you may use [OmniBB](https://gitlab.com/suborg/omnibb) to install it if missing)
- `2`: Call recording on/auto/off (works on KaiOS 2.5.2 and higher, tested on Nokia 2720 Flip and Nokia 800 Tough)
- `3`: Install application package (OmniSD/GerdaPkg compatible, works when developer menu is enabled, tested on Nokias only)
- `4`: Override TTL when tethering until reboot (Qualcomm devices only)
- `5`: Change IMEI1 with a random IMEI (Nokia)
- `6`: Change IMEI2 with a random IMEI (Nokia)
- `7`: Toggle browser proxy on/off
- `8`: Set browser proxy host and port
- `9`: Override user agent (dangerous: affects KaiStore accessibility, can't be reset until the factory reset or manual device preferences editing in WebIDE)
- `0`: Toggle diagnostics port (Qualcomm devices only)
- `*`: Run overclocking script (Qualcomm devices only)
- `#`: Enable developer menu and privileged access (via cache injection method)
- `Call`: Edit Wi-Fi MAC address (Nokia and MediaTek devices only, temporary for MediaTeks)
- Left soft key: Edit Bluetooth MAC address (Nokia devices only)
- Right soft key: Make all pre-installed apps removable from the app menu without affecting system partition (requires Busybox for the operation)


## IMEI Generation explains
It will generate a valid IMEI associated to one of the following phone model
   * iPhone X,  iPhone 11,  iPhone 11 Pro, iPhone 12, iPhone 12 Pro Max, iPhone 13 mini, iPhone 13 Pro, iPhone 13 Pro Max, iPhone 14 Pro, Samsung Galaxy A14, Pixel 3a, Pixel 3a XL Pixel 4a, Pixel 6

We avoid using a pure random IMEI that could be associated to a non existing phone model. The IMEI choosen are from best selling smartphone to avoid making it suspicious.

To know more on how IMEI works, look at [here](https://en.wikipedia.org/wiki/Type_Allocation_Code) and [here](https://en.wikipedia.org/wiki/International_Mobile_Equipment_Identity)

## Installation
You'll need 
* a Nokia 8110
* a computer (only a GNU/Linux operating system is described here, but it can be done on Windows)
* an internet connection


### Nokia 8110 configuration

You need to enable the debug mode to communicate with the computer.
Press the key to make a call, and dial:

`*#*#33284#*#*` (33284 means DEBUG )


****It will quit dial mode and you'll see a new icon (a bug) in top menu of desktop screen****


### Laptop configuration
#### SDK installation
ADB (Android Debug Bridge) is needed to interract with the phone.

Open a terminal, paste the following line and press enter to execute (for Debian based GNU/Linux OS)

`sudo apt update && sudo apt install android-sdk`

#### Install WebIDE - palemoone 

WebIDE is used to install the wallace toolbox. An older version of palemoon (a firefox fork) works well.

1. Download (46MB) this old version of palemoon which support WebIDE:

[`http://archive.palemoon.org/palemoon/28.x/28.6.1/palemoon-28.6.1.linux-x86_64.tar.bz2`](http://archive.palemoon.org/palemoon/28.x/28.6.1/palemoon-28.6.1.linux-x86_64.tar.bz2)

2. Extract the archive and open the palemoon directory.
3. Open palemoon web browser by a double click on the `palemoon` file

WARNING: Refuse to update palemoon when asked.

#### Download Wallace toolbox
1. Download the modified Wallace toolbox 

[`https://0xacab.org/genkoda/wallace-toolbox/-/archive/master/wallace-toolbox-master.zip`](https://0xacab.org/genkoda/wallace-toolbox/-/archive/master/wallace-toolbox-master.zip)


2. Extract the archive in your working directory


### Installation 

0. Wire your phone to the computer with a micro-USB cable

1. Prepare the phone to comunicate with palemoon by opening a TCP socket.
On your computer: open a terminal, paste the following lines and press enter to execute

```
adb devices
adb forward tcp:6000 localfilesystem:/data/local/debugger-socket
``` 



2. In palemoon web browser, open WebIDE: go to `"Tools" -> "Web Developper" -> "WebIDE"` OR you can press the shortcut `Shift + F8`

3. Connect Palemoon WebIDE app to the phone: on the right of WebIDE window, click on `Remote Runtime` and make sure it's set to localhost:6000.
4. On the left of palemoon WebIDE window, click on `Open Packaged App`, and select the wallace-toolbox-master directory
5. Click on the play icon on top "install and run" to install it
    
Congratulation, it's done !



